# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140618061540) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0,  :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "categories", :force => true do |t|
    t.string "name"
  end

  create_table "external_galleries", :force => true do |t|
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "yacht_id"
    t.string   "external_gallery"
    t.string   "name"
  end

  add_index "external_galleries", ["yacht_id"], :name => "index_external_galleries_on_yacht_id"

  create_table "flags", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "interior_galleries", :force => true do |t|
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "interior_gallery"
    t.string   "name"
    t.integer  "yacht_id"
  end

  add_index "interior_galleries", ["yacht_id"], :name => "index_interior_galleries_on_yacht_id"

  create_table "yachts", :force => true do |t|
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.float    "beam"
    t.string   "builder"
    t.integer  "cabin"
    t.integer  "category_id"
    t.integer  "crew"
    t.text     "description"
    t.string   "designer"
    t.float    "draft"
    t.string   "engine"
    t.string   "flag"
    t.float    "grt"
    t.string   "guest"
    t.string   "hull"
    t.float    "length"
    t.string   "location"
    t.string   "name"
    t.string   "price"
    t.integer  "range"
    t.string   "speed"
    t.string   "summer"
    t.string   "winter"
    t.string   "year"
    t.string   "teaser_image"
    t.string   "cover_image"
    t.string   "external_image"
    t.string   "hex_code"
    t.string   "location_image"
    t.string   "flag_image"
    t.string   "destination"
    t.string   "uri"
    t.string   "charter_location_image"
    t.string   "destination_title"
  end

  add_index "yachts", ["category_id"], :name => "index_yachts_on_category_id"

end
