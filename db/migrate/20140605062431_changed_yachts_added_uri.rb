class ChangedYachtsAddedUri < ActiveRecord::Migration
  def self.up
    add_column :yachts, :uri, :string
  end
  
  def self.down
    remove_column :yachts, :uri
  end
end
