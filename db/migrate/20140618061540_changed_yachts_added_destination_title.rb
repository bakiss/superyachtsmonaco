class ChangedYachtsAddedDestinationTitle < ActiveRecord::Migration
  def self.up
    add_column :yachts, :destination_title, :string
  end
  
  def self.down
    remove_column :yachts, :destination_title
  end
end
