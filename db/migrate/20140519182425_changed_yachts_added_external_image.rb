class ChangedYachtsAddedExternalImage < ActiveRecord::Migration
  def self.up
    add_column :yachts, :external_image, :string
  end
  
  def self.down
    remove_column :yachts, :external_image
  end
end
