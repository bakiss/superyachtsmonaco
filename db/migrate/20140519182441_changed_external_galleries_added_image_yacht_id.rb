class ChangedExternalGalleriesAddedImageYachtId < ActiveRecord::Migration
  def self.up
    add_column :external_galleries, :image, :string
    add_column :external_galleries, :yacht_id, :integer
    add_index :external_galleries, :yacht_id
  end
  
  def self.down
    remove_column :external_galleries, :image
    remove_column :external_galleries, :yacht_id
  end
end
