class ChangedCategoriesDeletedCreatedAtUpdatedAt < ActiveRecord::Migration
  def self.up
    remove_column :categories, :created_at
    remove_column :categories, :updated_at
  end
  
  def self.down
    add_column :categories, :created_at, :datetime, :limit=>nil, :default=>nil
    add_column :categories, :updated_at, :datetime, :limit=>nil, :default=>nil
  end
end
