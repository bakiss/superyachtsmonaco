class ChangedYachtsAddedFlagImage < ActiveRecord::Migration
  def self.up
    add_column :yachts, :flag_image, :string
  end
  
  def self.down
    remove_column :yachts, :flag_image
  end
end
