class ChangedInteriorGalleriesAddedInteriorGalleryNameYachtId < ActiveRecord::Migration
  def self.up
    add_column :interior_galleries, :interior_gallery, :string
    add_column :interior_galleries, :name, :string
    add_column :interior_galleries, :yacht_id, :integer
    add_index :interior_galleries, :yacht_id
  end
  
  def self.down
    remove_column :interior_galleries, :interior_gallery
    remove_column :interior_galleries, :name
    remove_column :interior_galleries, :yacht_id
  end
end
