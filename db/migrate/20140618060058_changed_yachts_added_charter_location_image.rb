class ChangedYachtsAddedCharterLocationImage < ActiveRecord::Migration
  def self.up
    add_column :yachts, :charter_location_image, :string
  end
  
  def self.down
    remove_column :yachts, :charter_location_image
  end
end
