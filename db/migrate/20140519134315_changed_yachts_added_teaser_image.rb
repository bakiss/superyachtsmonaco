class ChangedYachtsAddedTeaserImage < ActiveRecord::Migration
  def self.up
    add_column :yachts, :teaser_image, :string
  end
  
  def self.down
    remove_column :yachts, :teaser_image
  end
end
