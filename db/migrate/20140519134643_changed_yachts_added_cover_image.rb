class ChangedYachtsAddedCoverImage < ActiveRecord::Migration
  def self.up
    add_column :yachts, :cover_image, :string
  end
  
  def self.down
    remove_column :yachts, :cover_image
  end
end
