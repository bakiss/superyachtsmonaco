class ChangedYachtsModifiedGuestSpeed < ActiveRecord::Migration
  def self.up
    change_column :yachts, :guest, :string
    change_column :yachts, :speed, :string
  end
  
  def self.down
    change_column :yachts, :guest, :integer, :limit=>nil, :default=>nil
    change_column :yachts, :speed, :float, :limit=>nil, :default=>nil
  end
end
