class ChangedYachtsAddedHexCode < ActiveRecord::Migration
  def self.up
    add_column :yachts, :hex_code, :string
  end
  
  def self.down
    remove_column :yachts, :hex_code
  end
end
