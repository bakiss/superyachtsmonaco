class ChangedExternalGalleriesAddedNameAndDeletedImage < ActiveRecord::Migration
  def self.up
    add_column :external_galleries, :name, :string
    remove_column :external_galleries, :image
  end
  
  def self.down
    add_column :external_galleries, :image, :string, :limit=>255, :default=>nil
    remove_column :external_galleries, :name
  end
end
