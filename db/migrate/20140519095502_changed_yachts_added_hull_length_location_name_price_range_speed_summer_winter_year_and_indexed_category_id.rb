class ChangedYachtsAddedHullLengthLocationNamePriceRangeSpeedSummerWinterYearAndIndexedCategoryId < ActiveRecord::Migration
  def self.up
    add_column :yachts, :hull, :string
    add_column :yachts, :length, :float
    add_column :yachts, :location, :string
    add_column :yachts, :name, :string
    add_column :yachts, :price, :string
    add_column :yachts, :range, :integer
    add_column :yachts, :speed, :float
    add_column :yachts, :summer, :string
    add_column :yachts, :winter, :string
    add_column :yachts, :year, :string
    add_index :yachts, :category_id
  end
  
  def self.down
    remove_column :yachts, :hull
    remove_column :yachts, :length
    remove_column :yachts, :location
    remove_column :yachts, :name
    remove_column :yachts, :price
    remove_column :yachts, :range
    remove_column :yachts, :speed
    remove_column :yachts, :summer
    remove_column :yachts, :winter
    remove_column :yachts, :year
  end
end
