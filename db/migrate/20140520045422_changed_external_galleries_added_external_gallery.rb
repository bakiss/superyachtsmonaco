class ChangedExternalGalleriesAddedExternalGallery < ActiveRecord::Migration
  def self.up
    add_column :external_galleries, :external_gallery, :string
  end
  
  def self.down
    remove_column :external_galleries, :external_gallery
  end
end
