class ChangedYachtsAddedDestination < ActiveRecord::Migration
  def self.up
    add_column :yachts, :destination, :string
  end
  
  def self.down
    remove_column :yachts, :destination
  end
end
