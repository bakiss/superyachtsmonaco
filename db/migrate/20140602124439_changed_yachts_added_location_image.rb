class ChangedYachtsAddedLocationImage < ActiveRecord::Migration
  def self.up
    add_column :yachts, :location_image, :string
  end
  
  def self.down
    remove_column :yachts, :location_image
  end
end
