class Api::YachtController < ApplicationController

  def index

    yacht = Yacht.where("LOWER(uri) LIKE ?", params[:uri]).first    
    external_gallery = yacht.external_galleries.order("created_at ASC")
    interior_gallery = yacht.interior_galleries.order("created_at ASC")
    respond = {:yacht => yacht,
               :external_galleries => external_gallery,
               :interior_galleries => interior_gallery}
    respond_to do |format|
      format.json {render :json => respond}
    end
  end

  def sale
    #yachts = Category.where(:name => 'Sale').first.yachts.order("length DESC")
    yachts = []
    Category.where(:name => ['Sale', 'All']).each do | item |
           yachts += item.yachts
    end
    yachts.sort_by! {|item| item[:length]}.reverse!
    render json: yachts
  end

  def charter
    yachts = []
    Category.where(:name => ['Charter', 'All']).each do | item |
           yachts += item.yachts
    end
    yachts.sort_by! {|item| item[:length]}.reverse!

#    yachts = Category.where(:name => 'Charter').first.yachts.order("length DESC")
    render json: yachts
  end
end
