class Yacht < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_protected
  belongs_to :category  
  has_many  :external_galleries
  has_many  :interior_galleries

  mount_uploader :teaser_image, TeaserImageUploader
  mount_uploader :cover_image, CoverImageUploader

  mount_uploader :flag_image, FlagUploader
  mount_uploader :location_image, CoverImageUploader
  mount_uploader :charter_location_image, CoverImageUploader

  accepts_nested_attributes_for :external_galleries, :interior_galleries, :allow_destroy => true

  structure do
    name     :string
    uri      :string
    designer :string
    length   :float
    description :text
    builder  :string
    year     :string
    flag     :string
    cabin    :integer
    guest    :string
    draft    :float
    beam     :float
    speed    :string
    hull     :string
    grt      :float
    engine   :string
    location :string
    price    :string
    summer   :string
    winter   :string
    crew     :integer
    range    :integer
    hex_code  :string
    destination  :string
    destination_title :string

    flag_image       :string
    location_image       :string
    charter_location_image :string
    teaser_image       :string
    cover_image       :string
    external_image    :string



    timestamps
  end



end
