class InteriorGallery < ActiveRecord::Base
  attr_accessible  :image, :interior_gallery, :name
  belongs_to :yacht

  mount_uploader :interior_gallery, InteriorGalleryUploader

  structure do
  	name    :string
  	interior_gallery   :string
  end

end
