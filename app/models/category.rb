class Category < ActiveRecord::Base
  attr_protected  
  has_many :yachts

  structure do
  	name     :string

  end
end
