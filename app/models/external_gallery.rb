class ExternalGallery < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_accessible  :image, :external_gallery, :name

  belongs_to :yacht

  mount_uploader :external_gallery, ExternalGalleryUploader  

  structure do
  	name      :string
    external_gallery   :string  	

  end
  
end
