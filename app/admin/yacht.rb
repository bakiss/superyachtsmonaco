ActiveAdmin.register Yacht do
  filter :category
  form do |f|
    f.inputs  "General" do
      f.input  :name
      f.input  :uri
      f.input  :description
      f.input  :category
      f.input  :designer
      f.input  :builder
      f.input  :engine
      f.input  :draft
      f.input  :grt
      f.input  :speed
      f.input  :year
      f.input  :beam
      f.input  :length
      f.input  :hex_code
      f.input  :teaser_image
      f.input  :cover_image      
      f.input  :flag
      f.input  :flag_image
    end

    f.inputs  "Sale Yacht" do
      f.input  :guest
      f.input  :hull      
      f.input  :price      
      f.input  :location
      f.input  :location_image
    end

    f.inputs  "Charter Yacht" do
      f.input  :destination, :hint => "This text will not be shown. It will be used for link"
      f.input  :destination_title, :hint => "This text will be shown in destination"
      f.input  :charter_location_image
      f.input  :summer
      f.input  :winter
      f.input  :crew
      f.input  :range
      f.input  :cabin
    end

    f.has_many :external_galleries do |item|
      item.input :name      
      item.input :external_gallery, :hint => item.object.external_gallery.url()        
      item.input :_destroy, :as=>:boolean, :required => false, :label=>'Remove'

    end

    f.has_many :interior_galleries do |item|
      item.input :name
      item.input :interior_gallery, :hint => item.object.interior_gallery.url()        
      item.input :_destroy, :as=>:boolean, :required => false, :label=>'Remove'
    end

    f.actions
  end


  index do
    # selectable_column
    column :name
    column :category
    column :designer
    column :teaser_image do |t|
      image_tag t.teaser_image.url(:thumb)
    end

    default_actions
  end

  # show do
  #   attributes_table do
  #     row :name
  #     row :category
  #     row :designer
  #   end
  # end
end
